from flask import Flask, render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/prank")
def prank():
    return "Just a Prank Bro...   We dont really care"

@app.route("/highlights")
def matches():
    return render_template("highlights.html")
